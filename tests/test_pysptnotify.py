#!/usr/bin/env python3

from pathlib import Path

import pytest
from mpris2.types import Metadata_Map
from pysptnotify import cli, get_art_file, get_metadata, get_spotifyd_player, notify

fake_uri = "org.mpris.MediaPlayer2.spotifyd"
fake_art_uri = "https://example.org"


@pytest.fixture(autouse=True)
def mocked_get_players_uri(mocker):
    return mocker.patch("pysptnotify.get_players_uri")


@pytest.fixture(autouse=True)
def mocked_player(mocker):
    return mocker.patch("pysptnotify.Player")


@pytest.fixture(autouse=True)
def mocked_requests(mocker):
    return mocker.patch("pysptnotify.requests")


@pytest.fixture(autouse=True)
def mocked_notification(mocker):
    return mocker.patch("pysptnotify.Notification")


@pytest.fixture(autouse=True)
def fake_player_meta(mocker):
    player = mocker.Mock()
    player.Metadata = Metadata_Map(
        {
            Metadata_Map.TITLE: "Song Title",
            Metadata_Map.ARTIST: ["Artist 1", "Artist 2"],
            Metadata_Map.ALBUM: "Album Name",
            Metadata_Map.ART_URI: fake_art_uri,
        }
    )
    return player


class TestGetSpotifyDPlayer:
    def test_get_player_with_right_uri(self, mocked_get_players_uri, mocked_player):
        mocked_get_players_uri.return_value = [fake_uri]

        get_spotifyd_player()

        mocked_player.assert_called_once_with(
            dbus_interface_info={"dbus_uri": fake_uri}
        )

    def test_returns_none_if_missing_player(
        self, mocked_get_players_uri, mocked_player
    ):
        mocked_get_players_uri.return_value = []

        assert get_spotifyd_player() is None
        assert not mocked_player.called


class TestGetArtFile:
    def test_empty_string_if_error(self, mocked_requests):
        mocked_requests.get.side_effect = Exception

        assert get_art_file(fake_art_uri) == ""

    def test_creates_file_with_content(self, mocker, mocked_requests):
        content = b"lorem ipsum"
        response = mocker.Mock(content=content)
        mocked_requests.get.return_value = response

        art_file = get_art_file(fake_art_uri)

        assert art_file != ""
        with open(art_file) as f:
            assert f.read() == content.decode()

        Path(art_file).unlink()


class TestGetMetadata:
    def test_returns_metadata(self, fake_player_meta):
        assert get_metadata(fake_player_meta) == {
            "title": "Song Title",
            "artist": "Artist 1,Artist 2",
            "album": "Album Name",
            "art_uri": fake_art_uri,
        }

    def test_injects_file_name(self, mocker, mocked_requests, fake_player_meta):
        content = b"lorem ipsum"
        response = mocker.Mock(content=content)
        mocked_requests.get.return_value = response

        assert get_metadata(fake_player_meta) == {
            "title": "Song Title",
            "artist": "Artist 1,Artist 2",
            "album": "Album Name",
            "art_uri": fake_art_uri,
            "art_file": mocker.ANY,
        }


class TestNotify:
    def test_notify_without_art_file(self, mocked_notification):
        metadata = {
            "title": "song title",
            "artist": "artist name",
            "album": "album",
        }
        notify(metadata)
        mocked_notification.assert_called_once_with(
            title="song title - artist name",
            description="album",
            duration=10,
            urgency="normal",
        )

    def test_notify_with_art_file(self, mocked_notification):
        metadata = {
            "title": "song title",
            "artist": "artist name",
            "album": "album",
            "art_file": "/tmp/foo",
        }
        notify(metadata)
        mocked_notification.assert_called_once_with(
            title="song title - artist name",
            description="album",
            duration=10,
            urgency="normal",
            icon_path="/tmp/foo",
        )


class TestCli:
    def test_exits_with_error_if_no_player(self, mocked_get_players_uri):
        mocked_get_players_uri.return_value = []
        with pytest.raises(SystemExit) as error:
            cli()
        assert error.value.code == 1

    def test_notifies_with_metadata(
        self,
        mocked_get_players_uri,
        mocked_notification,
        mocked_player,
        fake_player_meta,
    ):
        mocked_get_players_uri.return_value = [fake_uri]
        mocked_player.return_value = fake_player_meta
        cli()
        mocked_notification.assert_called_once_with(
            title="Song Title - Artist 1,Artist 2",
            description="Album Name",
            duration=10,
            urgency="normal",
        )

#!/usr/bin/env python3

import sys
from tempfile import NamedTemporaryFile

import requests
from mpris2 import Player, get_players_uri
from pynotifier import Notification


def get_spotifyd_player():
    players = list(player for player in get_players_uri(".+spotifyd"))
    if len(players) < 1:
        return None

    uri = players[0]
    player = Player(dbus_interface_info={"dbus_uri": uri})
    return player


def get_art_file(uri):
    tmp = NamedTemporaryFile(delete=False)
    file_name = tmp.name

    try:
        response = requests.get(uri)
        response.raise_for_status()
        tmp.write(response.content)
    except Exception:
        file_name = ""
    finally:
        tmp.close()

    return file_name


def get_metadata(player):
    _meta = player.Metadata
    artists = [str(a) for a in _meta.get(_meta.ARTIST, [])]
    metadata = {
        "title": str(_meta.get(_meta.TITLE)),
        "artist": ",".join(artists),
        "album": str(_meta.get(_meta.ALBUM)),
        "art_uri": str(_meta.get(_meta.ART_URI)),
    }
    if art_file := get_art_file(metadata["art_uri"]):
        metadata["art_file"] = art_file

    return metadata


def notify(metadata):
    kwargs = {
        "title": f"{metadata.get('title')} - {metadata.get('artist')}",
        "description": metadata.get("album"),
        "duration": 10,
        "urgency": "normal",
    }

    if art_file := metadata.get("art_file"):
        kwargs["icon_path"] = art_file

    Notification(**kwargs).send()


def cli():
    player = get_spotifyd_player()
    if not player:
        sys.exit(1)

    notify(get_metadata(player))

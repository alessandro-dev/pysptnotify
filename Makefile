CMD = poetry run
VENV := $(shell poetry env info --path)

lint:
	$(CMD) flake8
	$(CMD) black --check pysptnotify
	$(CMD) isort --check pysptnotify

install:
	poetry install
ifdef CI
	ln -sf $(VENV)/bin/pysptnotify $(HOME)/.local/bin/pysptnotify
endif

test:
	$(CMD) pytest -s --cov=pysptnotify
